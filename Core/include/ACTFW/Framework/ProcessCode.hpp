// This file is part of the ACTS project.
//
// Copyright (C) 2017 ACTS project team
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//
//  ProcessCode.h
//  ACTFW
//
//  Created by Andreas Salzburger on 11/05/16.
//
//

#ifndef ACTFW_FRAMEWORK_PROCESSCODE_H
#define ACTFW_FRAMEWORK_PROCESSCODE_H 1

namespace FW {
enum class ProcessCode { SUCCESS, ABORT, END };
}

#endif  // ACTFW_FRAMEWORK_PROCESSCODE_H
